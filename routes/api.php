<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/auth/login1', function () {
    return view('welcome');
});

Route::post('/auth/register', [UserController::class, 'create']);
Route::post('/login', [UserController::class, 'login'])->name('login');
// Route::get('/login', [UserController::class, 'me']);
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/me', function(Request $request) {
        return auth()->user();
    });
    
});
Route::post('/logout', [UserController::class, 'logout']);

