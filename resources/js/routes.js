import Vue from 'vue';
import Users from './components/UserComponent.vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/users', 
    component: Users }
  ]
})
export default router